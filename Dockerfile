FROM debian:bullseye

ENV LANG=en_US.UTF-8 \
	LANGUAGE=en_US:en \
	TZ=US/Eastern

RUN set -eux; \
	apt-get -y update; \
	apt-get -y install clamav-daemon clamav-freshclam less locales ncat; \
	apt-get -y clean; \
	rm -rf /var/lib/apt/lists/*; \
	echo "$LANG UTF-8" >> /etc/locale.gen; \
	locale-gen $LANG; \
	echo 'TCPSocket 3310' > /etc/clamav/clamd-docker.conf

RUN freshclam

EXPOSE 3310

HEALTHCHECK --start-period=15s --retries=5 \
	CMD echo 'PING' | ncat localhost 3310

CMD ["/usr/sbin/clamd", "-F", "-c", "/etc/clamav/clamd-docker.conf"]

USER clamav
