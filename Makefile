SHELL = /bin/bash

build_tag ?= clamav

.PHONY: build
build:
	DOCKER_BUILDKIT=1 docker build --pull -t $(build_tag) - < ./Dockerfile

.PHONY: clean
clean:
	echo 'no-op'

.PHONY: test
test:
	build_tag=$(build_tag) ./test.sh
