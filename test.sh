#!/bin/bash

echo "Starting test image ..."
container="clamav-${CI_COMMIT_SHORT_SHA:-test}"
docker run --rm -d --name ${container} ${build_tag}

code=1
interval=5
timeout=120
SECONDS=0
while [[ $SECONDS -lt $timeout ]]; do
    echo -n "Checking health status ... "
    status=$(docker inspect -f '{{.State.Health.Status}}' $container)
    echo $status
    case $status in
        healthy)
	    code=0
            break
            ;;
        unhealthy)
            break
            ;;
        starting)
            sleep $interval
            ;;
        *)
            echo "Unexpected status."
            code=1
            break
            ;;
    esac
done

echo "Cleaning up ..."
docker stop $container

if [[ $code -eq 0 ]]; then
    echo "SUCCESS"
else
    echo "FAILURE"
fi

exit $code
